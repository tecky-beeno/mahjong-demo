let comboSet17 = symbolArrayToTileArray([
  ['d2', 4],
  ['b2', 4],
  ['c5', 4],
  ['e', 3],
  ['fa', 2],
]);
console.log({ comboSet17 });

let serialSet14 = symbolArrayToTileArray([
  'b123',
  'b456',
  'b789',
  'b789',
  ['b1', 2],
]);
console.log({ serialSet14 });

function symbolArrayToTileArray(comboSet) {
  let tiles = [];
  for (let element of comboSet) {
    if (Array.isArray(element)) {
      // e.g. ['d2', 4] means repeat 'd2' 4 times
      let [symbol, repeat] = element;
      for (let i = 0; i < repeat; i++) {
        let tile = symbolToTile(symbol);
        tiles.push(tile);
      }
    } else if (typeof element === 'string') {
      // e.g. 'd123' means 'd1', 'd2', 'd3'
      let type = element[0];
      for (let i = 1; i < element.length; i++) {
        let value = element[i];
        let symbol = type + value;
        let tile = symbolToTile(symbol);
        tiles.push(tile);
      }
    } else {
      throw new Error('unsupported format');
    }
  }
  return tiles;
}

function symbolToTile(symbol) {
  switch (symbol) {
    case 'e':
    case 's':
    case 'w':
    case 'n':
      return { type: 'dir', value: symbol };
  }
  switch (symbol) {
    case 'fa':
    case 'white':
    case 'mid':
      return { type: 'dragon', value: symbol };
  }
  return {
    type: symbolToType(symbol),
    value: symbol[1],
  };
}

function symbolToType(symbol) {
  switch (symbol[0]) {
    case 'd':
      return 'dot';
    case 'c':
      return 'char';
    case 'b':
      return 'bamboo';
  }
}
