let comboSet17 = symbolArrayToTileArray([
  ['d2', 4],
  ['b2', 4],
  ['c5', 4],
  ['e', 3],
  ['fa', 2],
]);
console.log(comboSet17);

function symbolArrayToTileArray(comboSet) {
  let tiles = [];
  for (let [symbol, repeat] of comboSet) {
    for (let i = 0; i < repeat; i++) {
      let tile = symbolToTile(symbol);
      tiles.push(tile);
    }
  }
  return tiles;
}

function symbolToTile(symbol) {
  switch (symbol) {
    case 'e':
    case 's':
    case 'w':
    case 'n':
      return { type: 'dir', value: symbol };
  }
  switch (symbol) {
    case 'fa':
    case 'white':
    case 'mid':
      return { type: 'dragon', value: symbol };
  }
  return {
    type: symbolToType(symbol),
    value: symbol[1],
  };
}

function symbolToType(symbol) {
  switch (symbol[0]) {
    case 'd':
      return 'dot';
    case 'c':
      return 'char';
    case 'b':
      return 'bamboo';
  }
}
